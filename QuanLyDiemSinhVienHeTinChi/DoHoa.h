﻿#pragma once
#include "mylib.h"
void box(int startX, int startY, int cao, int rong) {
	//Vẽ cạnh ngang
	for (int ix = startX; ix < startX + rong; ix++) {
		//Vẽ cạnh trên
		gotoxy(ix, startY);
		cout << char(196); // ─
		//Vẽ cạnh dưới
		gotoxy(ix, startY + cao);
		cout << char(196); // ─
	}
	//Vẽ cạnh dọc
	for (int iy = startY; iy <= startY + cao; iy++) {
		//Vẽ cạnh trái
		gotoxy(startX, iy);
		cout << char(179); // │
		//Vẽ cạnh phải
		gotoxy(startX + rong, iy);
		cout << char(179); // │
	}
	//Vẽ góc trên trái
	gotoxy(startX, startY); cout << char(218); // ┌
	//Vẽ góc dưới trái
	gotoxy(startX, startY + cao); cout << char(192); // └
	//Vẽ góc trên phải
	gotoxy(startX + rong, startY); cout << char(191); // ┐
	//Vẽ góc dưới phải
	gotoxy(startX + rong, startY + cao); cout << char(217); // ┘
}
void XuatDuLieu(int startX, int startY, int cao, int rong, string title[], int slData, string value[]) {
	int ix = startX, iy = startY + 1;
	int index = 0, khoangCanLe = 12;
	//---------- In title & box
	for (int i = 0; i < slData; i++) {
		gotoxy(ix, iy); cout << setw(khoangCanLe) << left << title[i]; // In title bằng 1 khoảng căn lề 
		box(ix + khoangCanLe, iy - 1, cao, rong); // In box, vẽ xong title tới đâu thì bắt đầu in box ở đó
		gotoxy(ix + khoangCanLe + 1, iy); cout << value[i];
		iy += cao + 1; // Cập nhật vị trí box tiếp theo
	}
	ShowCur(0);
}
string* NhapDuLieu(int startX, int startY, int cao, int rong, string title[], int slData, string value[], bool includeNum[], bool includeAlphabet[], int len[], char &key_press_return) {
	ShowCur(1);
	int ix = startX, iy = startY + 1;
	int index = 0, khoangCanLe = 12;
	//---------- In title & box
	for (int i = 0; i < slData; i++) {
		gotoxy(ix, iy); cout << setw(khoangCanLe) << left << title[i]; // In title bằng 1 khoảng căn lề 
		box(ix + khoangCanLe, iy - 1, cao, rong); // In box, vẽ xong title tới đâu thì bắt đầu in box ở đó
		iy += cao + 1; // Cập nhật vị trí box tiếp theo
	}
	startX = startX + khoangCanLe + 1; startY += 1; // Cập nhật lại vị trí con trỏ trong box đầu
	//Nút Lưu, Hủy
	gotoxy(startX, startY + (cao + 1) * slData); cout << "F1: Luu" << "\tESC: Huy";
	//==========================================
	if (value->empty()) {
		gotoxy(startX, startY); // Cho con trỏ về trong nội dung ô đầu
	}
	else {
		for (int i = 0; i < slData; i++) {
			gotoxy(startX, startY + (cao + 1) * i);
			cout << value[i];
		}
		index = slData - 1; //set vị trí con trỏ đã về cuối
	}
	//---------- Xử lý nhập dữ liệu
	char key_press = 0;
	//F1 để thoát và lưu dữ liệu đã nhập
	while (key_press != F1) {
		//Bắt phím 
		key_press = _getch();
		if (key_press == ESC) {
			for (int i = 0; i < slData; i++)
				value[i].clear();
			key_press_return = key_press;
			return value;
		}
		// Cấu hình hướng di chuyển khi gặp các phím 
		if (key_press == -32) {
			key_press = _getch();
			if (key_press == UP && index > 0) { //Phím mũi tên lên
				/*if (!AllowEdit[index - 1]) index -= 2;
				else index--;*/
				index--;
				//if (index == -1) index = slData - 1; //Lố 0 thì quay lại đỉnh
			}
			if (key_press == DOWN && index < slData - 1) { //Phím mũi tên xuống
				/*if (!AllowEdit[index + 1]) index += 2;
				else index++;*/
				index++;
				//if (index == slData) index = 0; //Lố đỉnh thì lại quay về 0
			}
			key_press = 0; //Trả về 0 để ko bị bắt các phím ký tự, do bắt 2 lần thì nó về lại mã trong pvi phím ký tự
		}
		else if (key_press == ENTER && index < slData - 1) {
			// y chan key DOWN hoiii
			/*if (!AllowEdit[index + 1]) index += 2;
			else index++;*/
			index++;
			//if (index == slData) index = 0; //Lố đỉnh thì lại quay về 0
		}
		else if (key_press == TAB && index < slData - 1) {
			/*if (!AllowEdit[index + 1]) index += 2;
			else index++;*/
			index++;
			//index = (index + 1) % slData;
		}
		//Di chuyển theo hướng đã cấu hình
		int offsetX = startX + value[index].length();
		int offsetY = startY + index * (cao + 1);
		gotoxy(offsetX, offsetY);
		//Cấu hình nhập chữ và số
		if ((key_press >= 'A' && key_press <= 'Z')
			|| (key_press >= 'a' && key_press <= 'z')
			|| (key_press >= '0' && key_press <= '9')
			|| (key_press == SPACE)) {
			if (value[index].length() == rong - 2 || value[index].length() == len[index]) {
				continue; //Max rộng của box rồi nhập quài
			}
			if (includeNum[index] && (key_press >= '0' && key_press <= '9')) {
				cout << key_press;
				value[index] += key_press;
			}
			else if (includeAlphabet[index] && !(key_press >= '0' && key_press <= '9')) {
				cout << key_press;
				value[index] += key_press;
			}
			else if (includeAlphabet[index] && includeNum[index]) {
				cout << key_press;
				value[index] += key_press;
			}
		}
		else if (key_press == BACKSPACE) {
			if (value[index].length() == 0) continue; //Còn gì đâu mà xóa 
			cout << "\b \b";
			value[index].erase(value[index].length() - 1); //Xóa đi ký tự ở cuối là length - 1
		}
	}
	ShowCur(0);
	key_press_return = key_press;
	return value;
}
void InBanner(ifstream& FileIn, string Path) {
	FileIn.open(Path, ios_base::in);
	if (!FileIn) {
		cout << "\nKhong mo duoc File";
		return;
	}
	gotoxy(0, 2);
	while (!FileIn.eof()) {
		string banner;
		getline(FileIn, banner);
		SetColor(3);
		cout << banner << '\n';
	}
	FileIn.close();
}
void MenuFrame() {
	SetColor(9); //Xanh
	//Vẽ cạnh dọc
	for (int i = 0; i < 46; i++) {
		gotoxy(0, i);
		cout << char(219);
		gotoxy(167, i);
		cout << char(219);
	}
	//Vẽ cạnh dọc giữa
	/*for (int i = 20; i < 39; i++) {
		gotoxy(50, i);
		cout << char(219);
	}*/
	//Vẽ cạnh ngang
	for (int i = 0; i < 168; i++) {
		gotoxy(i, 0);
		cout << char(219);
		/*gotoxy(i, 20);
		cout << char(219);*/
		gotoxy(i, 46);
		cout << char(219);
	}
	SetColor(15);
}
void ClearFrame1() {
	//Frame1 là khung bên trái trên
	for (int i = 1; i < 50; i++) {
		for (int j = 1; j < 10; j++) {
			gotoxy(i, j); cout << " ";
		}
	}
}
void ClearFrame2() {
	Normal();
	//Frame2 là khung bên trái dưới
	for (int i = 1; i < 50; i++) {
		for (int j = 11; j < 46; j++) {
			gotoxy(i, j); cout << " ";
		}
	}
}
void ClearFrame3() {
	//Frame1 là khung bên phải bự
	for (int i = 51; i < 167; i++) {
		for (int j = 1; j < 46; j++) {
			gotoxy(i, j); cout << " ";
		}
	}
}
void ClearAll() {
	for (int i = 1; i < 167; i++) {
		for (int j = 1; j < 46; j++) {
			gotoxy(i, j);
			cout << " ";
		}
	}
}
int MenuDong(int startX, int startY, string td[], int so_item) {
	ShowCur(0);
	//--------- In Menu và Highlight dòng đầu
	int ix = startX, iy = startY; //Gán đỡ để còn lưu lại vị trí ban đầu
	int index = 0; //Vị trí dòng đang highlight
	for (int i = 0; i < so_item; i++) {
		gotoxy(ix, iy); //Đi tới tọa độ yêu cầu
		cout << td[i];
		iy++; //Tăng iy là tăng dòng lên để xún dòng đó
	}
	gotoxy(startX, startY); HighLight(); cout << td[index]; Normal(); //Mặc định highlight dòng đầu
	//--------- Xử lý nhận phím để di chuyển thanh sáng
	char key_press = 0;
	while (key_press != ENTER) {
		key_press = _getch();
		if (key_press == ESC) {
			return 0;
		}
		if (key_press == -32) {
			key_press = _getch();
			if (key_press == DOWN && index < so_item - 1) {
				Normal(); gotoxy(startX, startY + index); cout << td[index];//Normal dòng hiện tại
				index++;
			}
			else if (key_press == UP && index > 0) {
				Normal(); gotoxy(startX, startY + index); cout << td[index];//Normal dòng hiện tại
				index--;
			}
		}
		gotoxy(startX, startY + index);
		HighLight();
		cout << td[index];
	}
	Normal(); //Trước khi thoát hàm thì trả hiện trạng cũ
	return index + 1;
}
void MenuFrameCap1() {
	Normal();
	SetColor(9); //Màu xanh 
	//Vẽ cạnh dọc
	for (int i = 0; i < 46; i++) {
		gotoxy(0, i);
		cout << char(219);
		gotoxy(167, i);
		cout << char(219);
	}
	//Vẽ cạnh ngang
	for (int i = 0; i < 168; i++) {
		gotoxy(i, 0);
		cout << char(219);
		gotoxy(i, 46);
		cout << char(219);
	}
	//Vẽ cột dọc giữa
	for (int i = 0; i < 46; i++) {
		gotoxy(50, i);
		cout << char(219);
	}
	//Vẽ cột ngang giữa nhỏ
	for (int i = 0; i < 50; i++) {
		gotoxy(i, 10);
		cout << char(219);
	}
	//=======================================================================
	Normal(); //Trước khi thoát hàm thì trả hiện trạng cũ
}
