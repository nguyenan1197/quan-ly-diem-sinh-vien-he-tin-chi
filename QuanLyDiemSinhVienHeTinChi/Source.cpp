﻿#include "DanhSachLopTinChi.h"
#include "DanhSachSinhVien.h"
#include "DanhSachLop.h"

int main() {
	//Cấu hình màn hình console
	SetConsoleDisplayMode(GetStdHandle(STD_OUTPUT_HANDLE), CONSOLE_FULLSCREEN_MODE, 0);
	ShowScrollBar(GetConsoleWindow(), SB_VERT, 0);
	//Khai báo ===============
	DanhSachLopTinChi dsltc; DANHSACHLOP dsl; DANHSACHSINHVIEN dssv;
	ifstream FileIn; ofstream FileOut;
	LoadFileDanhSachLopTinChi(FileIn, dsltc);
	LoadFileDanhSachLop(dsl, FileIn);
	LoadFileDanhSachSinhVien(dssv, FileIn);
	//========================
	while (true) {
		Normal();
		system("cls");
		string Path = "banner.txt";
		InBanner(FileIn, Path);
		MenuFrame();
		string td[] = { "DANH SACH LOP TIN CHI       ",
						"DANH SACH LOP               ",
						"DANH SACH MON HOC           ",
						"DANG KY/HUY DK LOP TIN CHI  " };
		int so_item = sizeof(td) / sizeof(td[0]);
		int luachon = MenuDong(70, 20, td, so_item);

		if (luachon == 0) break;
		else if (luachon == 1)
			XuLyMenuMoLopTinChi(dsltc, dssv, FileIn, FileOut);
		else if (luachon == 2)
			XuLyMenuDanhSachLop(dsl, dssv, dsltc);
		else if (luachon == 3)
			BaoLoi("Chuc nang dang phat trien");
		else if (luachon == 4) {
			DangKyLopTinChi(dssv, dsltc);
		}
	}
	GiaiPhongBoNhoDSLTC(dsltc);
	system("cls");
	return 0;
}