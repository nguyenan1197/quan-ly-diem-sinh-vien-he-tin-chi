﻿#pragma once
#include "DanhSachSinhVien.h"
#define MAXLTC 10000
using namespace std;

struct LopTinChi {
	int  MaLopTc;
	string MaMh = "";
	int NienKhoa = 0, HocKy = 0, Nhom = 0;
	int SvMin = 0, SvMax = 0;
	bool TrangThai; //0: Hủy lớp; 1: Mở
	DSSVDK dssvdk;
}; typedef struct LopTinChi LOPTINCHI;
struct DanhSachLopTinChi {
	LOPTINCHI* ds[MAXLTC];
	int n = 0;
};
int TimMaLtcMax(DanhSachLopTinChi dsltc) {
	if (dsltc.n == 0) return 0;
	int MaLtcMax = 0;
	for (int i = 0; i < dsltc.n; i++) {
		if (dsltc.ds[i]->MaLopTc > MaLtcMax)
			MaLtcMax = dsltc.ds[i]->MaLopTc;
	}
	return MaLtcMax;
}
int SearchLtc(DanhSachLopTinChi& dsltc, LOPTINCHI ltc) {
	for (int i = 0; i < dsltc.n; i++) {
		if (ltc.MaMh == dsltc.ds[i]->MaMh &&
			ltc.NienKhoa == dsltc.ds[i]->NienKhoa &&
			ltc.HocKy == dsltc.ds[i]->HocKy &&
			ltc.Nhom == dsltc.ds[i]->Nhom)
			return i;
	}
	return -1;
}
int Nhap1LopTinChi(LOPTINCHI& ltc, DanhSachLopTinChi dsltc) {
	ShowCur(1);
	string title[] = { "Ma MH: ", "NK: ", "HK: ", "Nhom: ", "Sv Min: ", "Sv Max: " };
	//set thuộc tính cho từng field
	bool includeNum[] = { true, true, true, true, true, true }; //Có chứa số 
	bool includeAlphabet[] = { true, false, false, false, false, false }; //Có chứa chữ
	//bool allowEdit[] = { false, true, true, true, true, true };
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	//==== gán dữ liệu trước để show ra khi cần nhập lại
	value[0] = ltc.MaMh;
	value[1] = to_string(ltc.NienKhoa);
	value[2] = to_string(ltc.HocKy);
	value[3] = to_string(ltc.Nhom);
	value[4] = to_string(ltc.SvMin);
	value[5] = to_string(ltc.SvMax);
	for (int i = 1; i <= 5; i++) {
		if (value[i] == "0")
			value[i].clear();
	}
	//=================================================
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t     THEM LOP TIN CHI"; Normal();
	gotoxy(4, 18); SetColor(12);
	cout << setw(13) << left << "Ma LTC:"; cout << TimMaLtcMax(dsltc) + 1;
	box(16, 17, 2, 30); Normal();
	int len[slData] = { 7, 4, 1, 1, 3, 3 }; //Độ dài quy định của từng field
	char key_press = 0;
	NhapDuLieu(4, 20, 2, 30, title, slData, value, includeNum, includeAlphabet, len, key_press);
	ShowCur(0);
	//Nhập xong rồi thì gán dữ liệu nè
	bool CheckEmpty = true;
	for (int i = 0; i < slData; i++) {
		if (!value[i].empty()) {
			CheckEmpty = false;
			break;
		}
	}
	if (CheckEmpty == true) return -1;
	else {
		ltc.MaMh = value[0];
		ltc.NienKhoa = atoi(value[1].c_str());
		ltc.HocKy = atoi(value[2].c_str());
		ltc.Nhom = atoi(value[3].c_str());
		ltc.SvMin = atoi(value[4].c_str());
		ltc.SvMax = atoi(value[5].c_str());
		ltc.TrangThai = 1;
	}
	return 1;
}
int Nhap1LopTinChiDeXoa(LOPTINCHI& ltc, DanhSachLopTinChi dsltc) {
	ShowCur(1);
	string title[] = { "Ma MH: ", "NK: ", "HK: ", "Nhom: " };
	//set thuộc tính cho từng field
	bool includeNum[] = { true, true, true, true }; //Có chứa số 
	bool includeAlphabet[] = { true, false, false, false }; //Có chứa chữ
	//bool allowEdit[] = { false, true, true, true, true, true };
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	gotoxy(7, 18); SetColor(9);
	cout << "\t\t     XOA LOP TIN CHI"; Normal();
	int len[slData] = { 7, 4, 1, 1 }; //Độ dài quy định của từng field'
	char key_press = 0;
	NhapDuLieu(5, 20, 2, 30, title, slData, value, includeNum, includeAlphabet, len, key_press);
	ShowCur(0);
	//Nhập xong rồi thì gán dữ liệu nè
	if (value->empty()) {
		return -1;
	}
	else {
		ltc.MaMh = value[0];
		ltc.NienKhoa = atoi(value[1].c_str());
		ltc.HocKy = atoi(value[2].c_str());
		ltc.Nhom = atoi(value[3].c_str());
	}
	return 1;
}
int CapNhat1LopTinChi(LOPTINCHI& ltc) {
	ShowCur(1);
	string title[] = { "Ma MH: ", "NK: ", "HK: ", "Nhom: ", "Sv Min: ", "Sv Max: " };
	//set thuộc tính cho từng field
	bool includeNum[] = { true, true, true, true, true, true }; //Có chứa số 
	bool includeAlphabet[] = { true, false, false, false, false, false }; //Có chứa chữ
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	value[0] = ltc.MaMh;
	value[1] = to_string(ltc.NienKhoa);
	value[2] = to_string(ltc.HocKy);
	value[3] = to_string(ltc.Nhom);
	value[4] = to_string(ltc.SvMin);
	value[5] = to_string(ltc.SvMax);
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t     CAP NHAT LOP TIN CHI"; Normal();
	gotoxy(5, 18); SetColor(12); cout << setw(13) << left << "Ma LTC: " << ltc.MaLopTc;
	box(17, 17, 2, 30); Normal();
	int len[slData] = { 7, 4, 1, 1, 3, 3 }; //Độ dài quy định của từng field
	char key_press = 0;
	NhapDuLieu(5, 20, 2, 30, title, slData, value, includeNum, includeAlphabet, len, key_press);
	ShowCur(0);
	//Nhập xong rồi thì gán dữ liệu nè
	//Trước khi gán thì kiểm tra coi nhập đủ chưa, chưa thì bắt nhập lại
	bool CheckEmpty = true;
	for (int i = 0; i < slData; i++) {
		if (!value[i].empty()) {
			CheckEmpty = false;
			break;
		}
	}
	if (CheckEmpty == true) return -1;
	ltc.MaMh = value[0];
	ltc.NienKhoa = atoi(value[1].c_str());
	ltc.HocKy = atoi(value[2].c_str());
	ltc.Nhom = atoi(value[3].c_str());
	ltc.SvMin = atoi(value[4].c_str());
	ltc.SvMax = atoi(value[5].c_str());
	ltc.TrangThai = 1;
	return 1;
}
void Xuat1LopTinChi(LOPTINCHI ltc) {
	cout << setw(10) << left << ltc.MaLopTc;
	cout << setw(15) << left << ltc.MaMh;
	cout << setw(15) << left << ltc.NienKhoa;
	cout << setw(10) << left << ltc.HocKy;
	cout << setw(10) << left << ltc.Nhom;
	cout << setw(12) << left << ltc.SvMin;
	cout << setw(12) << left << ltc.SvMax;
	if (ltc.TrangThai == 0)
		cout << setw(15) << left << "Huy lop";
	else 
		cout << setw(15) << left << "Mo";
	cout << setw(5) << left << "Xem";
}
void XuatThongTinLopTinChiFrame2(LOPTINCHI ltc) {
	string title[] = { "Ma MH: ", "NK: ", "HK: ", "Nhom: " };
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	value[0] = ltc.MaMh;
	value[1] = to_string(ltc.NienKhoa);
	value[2] = to_string(ltc.HocKy);
	value[3] = to_string(ltc.Nhom);
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t   THONG TIN LOP TIN CHI"; Normal();
	XuatDuLieu(5, 18, 2, 30, title, slData, value);
}
void HeaderDanhSachLopTinChi() {
	SetColor(9); gotoxy(100, 2); cout << "DANH SACH LOP TIN CHI"; Normal();
	box(54, 3, 2, 109); box(54, 5, 38, 109); //Vẽ 2 box trên và dưới
	gotoxy(54, 5); cout << char(195); // Làm đẹp kk
	gotoxy(54 + 109, 5); cout << char(180); // Làm đẹp kk
	gotoxy(55, 4);
	cout << setw(10) << left << "MA LTC";
	cout << setw(15) << left << "MA MH";
	cout << setw(15) << left << "NIEN KHOA";
	cout << setw(10) << left << "HOC KY";
	cout << setw(10) << left << "NHOM";
	cout << setw(12) << left << "SV MIN";
	cout << setw(12) << left << "SV MAX";
	cout << setw(15) << left << "TRANG THAI";
	cout << setw(5) << left << "DSSV";
}
void XuatDanhSachLopTinChi(DanhSachLopTinChi dsltc) {
	//Thêm vô thì xuất mượt rồi đó, còn xóa thì chưa co được nè
	HeaderDanhSachLopTinChi();
	int const x = 55; int y = 6;
	for (int i = 0; i < dsltc.n; i++) {
		gotoxy(x, y++);
		Xuat1LopTinChi(*dsltc.ds[i]);
	}
}
void HeaderDanhSachLopTinChiDeDangKy(int nk, int hk) {
	SetColor(9); gotoxy(75, 2); cout << "DANH SACH LOP TIN CHI NIEN KHOA " << nk << " HOC KY " << hk; Normal();
	box(54, 3, 2, 86); box(54, 5, 38, 86); //Vẽ 2 box trên và dưới
	gotoxy(54, 5); cout << char(195); // Làm đẹp kk
	gotoxy(54 + 86, 5); cout << char(180); // Làm đẹp kk
	gotoxy(55, 4);
	cout << setw(15) << left << "MA MH";
	cout << setw(30) << left << "TEN MON HOC";
	cout << setw(10) << left << "NHOM";
	cout << setw(15) << left << "CON TRONG";
	cout << setw(15) << left << "TRANG THAI";
}
void Xuat1LopTinChiDeDangKy(LOPTINCHI ltc) {
	cout << setw(15) << left << ltc.MaMh;
	cout << setw(30) << left << "TEN MON HOC";
	cout << setw(10) << left << ltc.Nhom;
	cout << setw(15) << left << ltc.SvMax - ltc.dssvdk.n;
	if (ltc.TrangThai == 0)
		cout << setw(15) << left << "Huy lop";
	else
		cout << setw(15) << left << "Mo";
}
void XuatDanhSachLopTinChiDeDangKy(DanhSachLopTinChi dsltc) {
	ClearFrame3();
	HeaderDanhSachLopTinChiDeDangKy(dsltc.ds[0]->NienKhoa, dsltc.ds[0]->HocKy);
	int const x = 55; int y = 6;
	for (int i = 0; i < dsltc.n; i++) {
		gotoxy(x, y++);
		Xuat1LopTinChiDeDangKy(*dsltc.ds[i]);
	}
}
void ChuanHoaMaMh(string& MaMh) {
	//Xóa khoảng trắng đầu
	while (MaMh[0] == ' ')
		MaMh.erase(MaMh.begin() + 0);
	//Xóa khoảng trắng cuối
	while (MaMh[MaMh.length() - 1] == ' ')
		MaMh.erase(MaMh.begin() + MaMh.length() - 1);
	//Xóa toàn bộ khoảng trắng giữa
	for (int i = 0; i < MaMh.length(); i++) {
		//Xóa toàn bộ khoảng trắng giữa
		if (MaMh[i] != ' ' && MaMh[i + 1] == ' ') {
			MaMh.erase(MaMh.begin() + i + 1);
			i--;
		}
		//Sẵn ké nếu là chữ thường thì in hoa
		if (MaMh[i] >= 97 && MaMh[i] <= 122) {
			MaMh[i] -= 32;
		}
	}
}
void Them1LopTinChiVaoDanhSach(DanhSachLopTinChi& dsltc, LOPTINCHI ltc) {
	//ltc.MaLopTc = TimMaLtcMax(dsltc) + 1;
	//Chuẩn hóa Mã môn học 
	ChuanHoaMaMh(ltc.MaMh);
	dsltc.ds[dsltc.n] = new LOPTINCHI;
	*dsltc.ds[dsltc.n] = ltc;
	dsltc.n++;
}
int LoadFileDanhSachLopTinChi(ifstream& FileIn, DanhSachLopTinChi& dsltc) {
	//Xử lý mở file 
	string FileName = "danhsachloptinchi.txt";
	FileIn.open(FileName, ios_base::in);
	if (!FileIn) {
		BaoLoi("Khong tim thay File!");
		FileIn.close();
		exit(0);
	}
	if (FileEmpty(FileIn)) {
		FileIn.close();
		return 0;
	}
	//Đọc file vào danh sách
	while (!FileIn.eof()) {
		//Đọc vô lớp tín chỉ x, x là lớp tín chỉ đệm, rồi thêm x vô danh sách
		LOPTINCHI x;
		FileIn >> x.MaLopTc;
		FileIn >> x.MaMh; //getline(FileIn, x.MaMh, '\t');
		FileIn >> x.NienKhoa;
		FileIn >> x.HocKy;
		FileIn >> x.Nhom;
		FileIn >> x.SvMin;
		FileIn >> x.SvMax;
		FileIn >> x.TrangThai;
		FileIn.ignore();
		// Đọc vô x rồi check cái đã rồi mới thêm vô danh sách
		if (x.MaLopTc < 0 || x.NienKhoa < 2022 || x.Nhom <= 0 || x.SvMin <= 0 || x.SvMin > x.SvMax) {
			BaoLoi("Loi du lieu, xin kiem tra lai!");
			FileIn.close();
			exit(0);
		}
		if (SearchLtc(dsltc, x) != -1) {
			BaoLoi("Loi File, du lieu bi trung!");
			FileIn.close();
			exit(0);
		}
		//Check xong gán lớp tín chỉ x cho thành phần trong danh sách
		Them1LopTinChiVaoDanhSach(dsltc, x);
	}
	FileIn.close();
	return 0;
}
void SaveFileDanhSachLopTinChi(ofstream& FileOut, DanhSachLopTinChi& dsltc) {
	string FileName = "danhsachloptinchi.txt";
	FileOut.open(FileName, ios_base::out);
	for (int i = 0; i < dsltc.n; i++) {
		FileOut << dsltc.ds[i]->MaLopTc; FileOut << '\t';
		FileOut << dsltc.ds[i]->MaMh; FileOut << '\t';
		FileOut << dsltc.ds[i]->NienKhoa; FileOut << '\t';
		FileOut << dsltc.ds[i]->HocKy; FileOut << '\t';
		FileOut << dsltc.ds[i]->Nhom; FileOut << '\t';
		FileOut << dsltc.ds[i]->SvMin; FileOut << '\t';
		FileOut << dsltc.ds[i]->SvMax; FileOut << '\t';
		FileOut << dsltc.ds[i]->TrangThai;
		if (i < dsltc.n - 1) FileOut << '\n';
	}
	FileOut.close();
}
DanhSachLopTinChi SearchLtcByMaMh(DanhSachLopTinChi dsltc, string MaMh) {
	DanhSachLopTinChi dsltcTemp;
	for (int i = 0; i < dsltc.n; i++) {
		if (dsltc.ds[i]->MaMh == MaMh)
			Them1LopTinChiVaoDanhSach(dsltcTemp, *dsltc.ds[i]);
	}
	return dsltcTemp;
}
int SearchLtcByMaLtc(DanhSachLopTinChi dsltc, int MaLtc) {
	for (int i = 0; i < dsltc.n; i++) {
		if (dsltc.ds[i]->MaLopTc == MaLtc)
			return i;
	}
	return -1;
}
void Xoa1LopTinChiRaKhoiDanhSach(DanhSachLopTinChi& dsltc, int MaLtc) {
	int vt = SearchLtcByMaLtc(dsltc, MaLtc);
	delete dsltc.ds[vt];
	for (int i = vt; i < dsltc.n - 1; i++)
		dsltc.ds[i] = dsltc.ds[i + 1];
	dsltc.n--;
	//Xử lý giao diện
	//Xóa hết mấy dòng dưới dòng lớp tín chỉ vừa xóa
	while (vt < dsltc.n + 1) {
		if (vt == dsltc.n) {
			for (int i = 55; i < 55 + 104; i++) {
				gotoxy(i, 6 + vt); cout << " ";
			}
			break;
		}
		vt++;
		for (int i = 55; i < 55 + 104; i++) {
			gotoxy(i, 6 + vt); cout << " ";
		}
	}
	XuatDanhSachLopTinChi(dsltc);
}
void XoaLtcKoXuLyGiaoDien(DanhSachLopTinChi& dsltc, int MaLtc) {
	int vt = SearchLtcByMaLtc(dsltc, MaLtc);
	delete dsltc.ds[vt];
	for (int j = vt; j < dsltc.n - 1; j++)
		dsltc.ds[j] = dsltc.ds[j + 1];
	dsltc.n--;
}
bool SoSanh2Ltc(LOPTINCHI a, LOPTINCHI b) {
	if (a.MaMh == b.MaMh &&
		a.NienKhoa == b.NienKhoa &&
		a.HocKy == b.HocKy &&
		a.Nhom == b.Nhom &&
		a.SvMin == b.SvMin &&
		a.SvMax == b.SvMax) return true;
	return false;
}
void XuLyMenuMoLopTinChi(DanhSachLopTinChi& dsltc, DANHSACHSINHVIEN dssv, ifstream& FileIn, ofstream& FileOut) {
	//=========== Trước khi vô 1 chức năng thì clear all và phân lại màn hình
	ClearAll(); //Xóa rỗng chừa khung
	MenuFrameCap1();
	bool flag = true; 
	while (flag) {
		ClearFrame2();
		XuatDanhSachLopTinChi(dsltc);
		string td[] = { "MO LOP TIN CHI      ",
						"CAP NHAT LOP TIN CHI",
						"XOA LOP TIN CHI     ",
						"XEM DSSV DANG KY    ",
						"HUY LOP TIN CHI     ",
						"NHAP DIEM           "};
		int so_item = sizeof(td) / sizeof(td[0]);
		int luachon = MenuDong(3, 3, td, so_item);
		if (luachon == 0) {
			do {
				ClearFrame2(); Normal();
				gotoxy(3, 44); cout << "Ban co muon luu du lieu truoc khi thoat ? (y/n)";
				luachon = _getch();
				if (luachon == 'y' || luachon == 'Y') {
					SaveFileDanhSachLopTinChi(FileOut, dsltc);
					ClearFrame2();
					BaoLoi("Luu thanh cong!");
				}
				else if (luachon == 'n' || luachon == 'N') break;
				else {
					ClearFrame2();
					BaoLoi("Lua chon khong hop le!");
				}
			} while (luachon != 'y' && luachon != 'Y' && luachon != 'n' && luachon != 'N');
			flag = false;
		}
		else if (luachon == 1) {
			//Thêm lớp tín chỉ
			LOPTINCHI ltc;
			do {
				//ClearFrame2();
				if (Nhap1LopTinChi(ltc, dsltc) != -1) {
					if (SearchLtc(dsltc, ltc) != -1) {
						BaoLoi("Lop tin chi da ton tai!");
					}
					else {
						if (ltc.SvMin > ltc.SvMax) 
							BaoLoi("Sv Min phai nho hon Sv Max");
						else if (ltc.MaMh == "") 
							BaoLoi("Ma MH khong duoc bo trong!");
						else if (ltc.HocKy == 0) 
							BaoLoi("Hoc ky khong duoc bo trong!");
						else if (ltc.Nhom == 0) 
							BaoLoi("Nhom khong duoc bo trong!");
						else if (ltc.SvMin == 0) 
							BaoLoi("Sv Min khong duoc bo trong!");
						else if (ltc.SvMax == 0) 
							BaoLoi("Sv Max khong duoc bo trong");
						else {
							ltc.MaLopTc = TimMaLtcMax(dsltc) + 1; // Nhập riêng để load file ko bị sai mã lớp tín chỉ
							Them1LopTinChiVaoDanhSach(dsltc, ltc);
							BaoLoi("Them thanh cong!");
							break;
						}
					}
				}
				else break;
			} while (SearchLtc(dsltc, ltc) != -1 || ltc.SvMin > ltc.SvMax ||
				     ltc.MaMh == "" || ltc.HocKy == 0 ||
					 ltc.Nhom || ltc.SvMin == 0 || ltc.SvMax == 0);
		}
		else if (luachon == 2) {
			ClearFrame2();
			//Cập nhật lớp tín chỉ
			if (dsltc.n == 0) {
				BaoLoi("Danh sach lop tin chi rong!");
				continue;
			}
			int index = 0;
			gotoxy(55, 6);
			SetBGColor(2);
			Xuat1LopTinChi(*dsltc.ds[index]);
			char key_press_test = 0;
			while (key_press_test != ESC) {
				key_press_test = _getch();
				if (key_press_test == -32) {
					key_press_test = _getch();
					if (key_press_test == DOWN && index < dsltc.n - 1) {
						gotoxy(55, 6 + index);
						Normal();
						Xuat1LopTinChi(*dsltc.ds[index]);
						index++;
					}
					else if (key_press_test == UP && index > 0) {
						gotoxy(55, 6 + index);
						Normal();
						Xuat1LopTinChi(*dsltc.ds[index]);
						index--;
					}
				}
				gotoxy(55, 6 + index);
				SetBGColor(2);
				Xuat1LopTinChi(*dsltc.ds[index]);
				//Di chuyển thanh sáng gặp Enter thì thoát để xử lý
				if (key_press_test == ENTER) {
					Normal();
					//Kiểm tra lớp tín chỉ đã có sinh viên đk chưa, nếu chưa thì mới được cập nhật
					if (dsltc.ds[index]->dssvdk.n > 0) {
						BaoLoi("Lop tin chi da co SVDK, khong the cap nhat!");
						break;
					}
					//Xử lý ở đây
					//Riêng lựa chọn này thì tạo ra ltc temp để dùng, hết if thì temp cũng tự giải phóng, ko đụng chạm tránh ảnh hưởng con trỏ
					LOPTINCHI ltc_temp = *dsltc.ds[index]; //Hết sức lưu ý chỗ này có thể lỗi do lớp tín chỉ có con trỏ bên trong
					CapNhat1LopTinChi(ltc_temp);
					if (SoSanh2Ltc(ltc_temp, *dsltc.ds[index])) {
						BaoLoi("Khong co du lieu nao thay doi!");
						break;
					}
					if (SearchLtc(dsltc, ltc_temp) == -1) {
						// CAP NHAT 4 KEY
						if (ltc_temp.SvMin > ltc_temp.SvMax) {
							BaoLoi("Sv Min phai nho hon Sv Max");
							continue;
						}
						*dsltc.ds[index] = ltc_temp;  //Hết sức lưu ý chỗ này có thể lỗi do lớp tín chỉ có con trỏ bên trong
						BaoLoi("Cap nhat thanh cong!");
					}
					else {
						// CAP NHAT SV MAX SV MIN
						if ((ltc_temp.SvMax != dsltc.ds[index]->SvMax) || (ltc_temp.SvMin != dsltc.ds[index]->SvMin)) {
							if (ltc_temp.SvMin > ltc_temp.SvMax) {
								BaoLoi("Sv Min phai nho hon Sv Max");
								continue;
							}
							*dsltc.ds[index] = ltc_temp;  //Hết sức lưu ý chỗ này có thể lỗi do lớp tín chỉ có con trỏ bên trong
							BaoLoi("Cap nhat thanh cong!");
						}
						else BaoLoi("Du lieu bi trung, xin kiem tra lai!");
					}
					Normal();
					ClearFrame2();
					break;
				}
			}
		}
		else if (luachon == 3) {
			//Xóa lớp tín chỉ
			if (dsltc.n == 0) {
				BaoLoi("Danh sach lop tin chi rong!");
				continue;
			}
			int index = 0;
			gotoxy(55, 6);
			SetBGColor(2);
			Xuat1LopTinChi(*dsltc.ds[index]);
			char key_press_test = 0;
			while (key_press_test != ESC) {
				key_press_test = _getch();
				if (key_press_test == -32) {
					key_press_test = _getch();
					if (key_press_test == DOWN && index < dsltc.n - 1) {
						gotoxy(55, 6 + index);
						Normal();
						Xuat1LopTinChi(*dsltc.ds[index]);
						index++;
					}
					else if (key_press_test == UP && index > 0) {
						gotoxy(55, 6 + index);
						Normal();
						Xuat1LopTinChi(*dsltc.ds[index]);
						index--;
					}
				}
				gotoxy(55, 6 + index);
				SetBGColor(2);
				Xuat1LopTinChi(*dsltc.ds[index]);
				if (key_press_test == ENTER) 
					break;
			}
			Normal();
			if (index >= 0 && key_press_test != ESC) {
				char luachon = 0;
				do {
					ClearFrame2();
					if (dsltc.ds[index]->dssvdk.n > 0) {
						BaoLoi("Lop tin chi da co SVDK, khong the xoa!");
						break;
					}
					gotoxy(5, 44); cout << "Xac nhan xoa lop tin chi ? (y/n)";
					luachon = _getch();
					if (luachon == 'y' || luachon == 'Y') {
						Xoa1LopTinChiRaKhoiDanhSach(dsltc, dsltc.ds[index]->MaLopTc);
						ClearFrame2();
						BaoLoi("Xoa thanh cong!");
					}
					else if (luachon == 'n' || luachon == 'N') break;
					else BaoLoi("Lua chon khong hop le!");
				} while (luachon != 'y' && luachon != 'Y' && luachon != 'n' && luachon != 'N');
			}
		}
		else if (luachon == 4) {
			//Xem danh sách sinh viên đăng ký
			if (dsltc.n == 0) {
				BaoLoi("Danh sach lop tin chi rong!");
				continue;
			}
			int index = 0;
			gotoxy(55, 6);
			SetBGColor(2);
			Xuat1LopTinChi(*dsltc.ds[index]);
			char key_press_test = 0;
			while (key_press_test != ESC) {
				key_press_test = _getch();
				if (key_press_test == -32) {
					key_press_test = _getch();
					if (key_press_test == DOWN && index < dsltc.n - 1) {
						gotoxy(55, 6 + index);
						Normal();
						Xuat1LopTinChi(*dsltc.ds[index]);
						index++;
					}
					else if (key_press_test == UP && index > 0) {
						gotoxy(55, 6 + index);
						Normal();
						Xuat1LopTinChi(*dsltc.ds[index]);
						index--;
					}
				}
				gotoxy(55, 6 + index);
				SetBGColor(2);
				Xuat1LopTinChi(*dsltc.ds[index]);
				if (key_press_test == ENTER) 
					break;
			}
			Normal();
			if (key_press_test != ESC) {
				ClearFrame3();
				XuatThongTinLopTinChiFrame2(*dsltc.ds[index]);
				XuatDanhSachSinhVienDangKy(dsltc.ds[index]->dssvdk, dssv);
				_getch();
			}
		}
		else if (luachon == 5) {
			//Hủy lớp tín chỉ có svdk < sv min
			SetColor(12);
			gotoxy(3, 42); cout << "Xoa cac lop tin chi co SVDK < SV MIn";
			gotoxy(3, 43); cout << "Xac nhan xoa ? (y/n)";
			char key_press = 0;
			do {
				key_press = _getch();
				if (key_press == 'y' || key_press == 'Y') {
					for (int i = 0; i < dsltc.n; i++) {
						if (dsltc.ds[i]->dssvdk.n < dsltc.ds[i]->SvMin) {
							XoaLtcKoXuLyGiaoDien(dsltc, dsltc.ds[i]->MaLopTc);
							i--;
						}
					}
				}
				else if (key_press == 'n' || key_press == 'N') break;
				else BaoLoi("Lua chon khong hop le, xin kiem tra lai!");
			} while (key_press != 'n' && key_press != 'N' && key_press != 'y' && key_press != 'Y');
			if (key_press == 'n' || key_press == 'N') continue;
			ClearFrame2();
			ClearFrame3();
			XuatDanhSachLopTinChi(dsltc);
			BaoLoi("Xoa thanh cong!");
		}
		else if (luachon == 6) {
			//Nhập điểm
			bool flag1 = true;
			while (flag1) {
				ClearFrame3();
				ClearFrame2();
				//ClearFrame1();
				string title[] = { "NIEN KHOA:", "HOC KY:", "NHOM", "TEN MON HOC:" };
				const int soItem = sizeof(title) / sizeof(title[0]);
				string value[soItem];
				bool includeNum[] = { true, true, true, false }, includeAlphabet[] = { false, false, false, true };
				int len[soItem] = { 4, 1, 1, 28 };
				char key_press = 0;
				//Cho nhập thử để bắt điều kiện các field
				do {
					NhapDuLieu(2, 15, 2, 30, title, soItem, value, includeNum, includeAlphabet, len, key_press);
					if (key_press == ESC) {
						flag1 = false;
						break;
					}
					else if (key_press == F1 && value[0].empty())
						BaoLoi("Nien khoa khong duoc bo trong!");
					else if (key_press == F1 && value[1].empty())
						BaoLoi("Hoc ky khong duoc bo trong!");
				} while ((key_press == F1 && value[0].empty()) || (key_press == F1 && value[1].empty()));
				if (key_press  == ESC) continue;
				//Nhập dữ liệu ok rồi thì xử lý tiếp
				//Lọc ra danh sách sinh viên đã đăng ký
				
				//Lọc xong xuất danh sách vừa lọc

			
					//Di chuyển thanh sáng gặp Enter thì thoát để xử lý
					//if (key_press == ENTER)
			}
		}
	}
}
void GiaiPhongBoNhoDSLTC(DanhSachLopTinChi& dsltc) {
	for (int i = 0; i < dsltc.n; i++) {
		delete dsltc.ds[i];
	}
}
void DangKyLopTinChi(DANHSACHSINHVIEN dssv, DanhSachLopTinChi& dsltc) {
	//Giao diện
	ClearAll();
	MenuFrameCap1();
	//Trước khi đăng ký phải check xem dssv có rỗng không đã
	if (dssv.pHead == NULL)
		BaoLoi("DSSV rong, vui long them sv truoc!");
	else {
		//B1: Yêu cầu nhập MSSV
		bool flag = true;
		while (flag) {
			Normal();
			ClearFrame1();
			string title[] = { "MSSV:" };
			string value[1];
			bool includeNum[] = { true }, includeAlphabet[] = { true };
			int len[1] = { 10 };
			char key_press = 0;
			NhapDuLieu(2, 4, 2, 30, title, 1, value, includeNum, includeAlphabet, len, key_press);
			//Bắt điều kiện các field
			if (key_press == F1 && value[0].empty()) BaoLoi("MSSV khong duoc de trong!"); //Bỏ trống nhấn lưu
			else if (key_press == ESC) return; //Nhấn ESC
			else { //Còn lại thì xử lý B2
				//Trước khi vào bước 2 thì xét xem mssv có tồn tại hay không
				bool check = false;
				for (NodeSinhVien* k = dssv.pHead; k != NULL; k = k->pNext) {
					if (k->Data.MaSv == value[0]) {
						check = true;
						break;
					}
				}
				if (!check) {
					BaoLoi("MSSV khong ton tai, xin kiem tra lai!");
				}
				else { //Tồn tại thì tiếp vào bước 2
					//B2: Nhập niên khóa, học kỳ
					bool flag1 = true;
					while (flag1) {
						ClearFrame3();
						XuatDanhSachLopTinChi(dsltc);
						Normal();
						ClearFrame1();
						string title1[] = { "NIEN KHOA:", "HOC KY:" };
						string value1[2];
						bool includeNum1[] = { true, true }, includeAlphabet1[] = { false, false };
						int len1[2] = { 4, 1 };
						char key_press1 = 0;
						//Cho nhập thử để bắt điều kiện các field
						do {
							NhapDuLieu(2, 2, 2, 30, title1, 2, value1, includeNum1, includeAlphabet1, len1, key_press1);
							if (key_press1 == ESC) {
								flag1 = false;
								break;
							}
							else if (key_press1 == F1 && value1[0].empty())
								BaoLoi("Nien khoa khong duoc bo trong!");
							else if (key_press1 == F1 && value1[1].empty())
								BaoLoi("Hoc ky khong duoc bo trong!");
						} while ((key_press1 == F1 && value1[0].empty()) || (key_press1 == F1 && value1[1].empty()));
						if (key_press1 == ESC) continue;
						//Nhập dữ liệu ok rồi thì xử lý tiếp
						//Lọc ra danh sách các lớp tín chỉ MỞ theo NK và HK user nhập
						DanhSachLopTinChi dsltc_Theo_Dk_User;
						//Chỗ lọc này hơi chậm, cần xem lại
						for (int i = 0; i < dsltc.n; i++) {
							if ((dsltc.ds[i]->NienKhoa == atoi(value1[0].c_str())) &&
								(dsltc.ds[i]->HocKy == atoi(value1[1].c_str())) &&
								(dsltc.ds[i]->TrangThai == 1)) {
								Them1LopTinChiVaoDanhSach(dsltc_Theo_Dk_User, *dsltc.ds[i]);
							}
						}
						//Lọc xong xuất danh sách vừa lọc
						XuatDanhSachLopTinChiDeDangKy(dsltc_Theo_Dk_User); 
						//Xử lý thanh sáng
						int index = 0;
						gotoxy(55, 6); SetBGColor(2); Xuat1LopTinChiDeDangKy(*dsltc_Theo_Dk_User.ds[index]); //Thanh sáng ở dòng đầu
						//Di chuyển thanh sáng
						while (key_press1 != ESC) {
							key_press1 = _getch();
							if (key_press1 == -32) {
								key_press1 = _getch();
								if (key_press1 == DOWN && index < dsltc_Theo_Dk_User.n - 1) {
									gotoxy(55, 6 + index);
									Normal();
									Xuat1LopTinChiDeDangKy(*dsltc_Theo_Dk_User.ds[index]);
									index++;
								}
								else if (key_press1 == UP && index > 0) {
									gotoxy(55, 6 + index);
									Normal();
									Xuat1LopTinChiDeDangKy(*dsltc_Theo_Dk_User.ds[index]);
									index--;
								}
							}
							gotoxy(55, 6 + index);
							SetBGColor(2);
							Xuat1LopTinChiDeDangKy(*dsltc_Theo_Dk_User.ds[index]);
							//Di chuyển thanh sáng gặp Enter thì thoát để xử lý
							if (key_press1 == ENTER) {
								Normal();
								ClearFrame1();
								string td[] = { "DANG KY    ", 
												"HUY DANG KY" };
								int luachon = MenuDong(4, 4, td, 2);
								if (luachon == 0) break;
								else if (luachon == 1) {
									//ĐăNG Ký
									// Xét sinh viên đk chưa, chưa thì mới cho đk
									bool check1 = false;
									for (NodeSvDangKy* k = dsltc_Theo_Dk_User.ds[index]->dssvdk.pHead; k != NULL; k = k->pNext) {
										if (k->Data.MaSv == value[0]) {
											check1 = true;
											break;
										}
									}
									if (check1 == true) {
										BaoLoi("Sinh vien da dang ky lop nay!");
										break;
									}
									//Xét slot còn trống, nếu ko trống thì ko cho đăng ký nữa
									if (dsltc_Theo_Dk_User.ds[index]->SvMax - dsltc_Theo_Dk_User.ds[index]->dssvdk.n == 0) {
										BaoLoi("Lop tin chi da day, khong the dang ky!");
										break;
									}
									//Thêm sinh viên vào dsdk của ltc xong break
									SINHVIENDK svdk;
									svdk.Diem = 0;
									svdk.MaSv = value[0];
									svdk.TrangThai = 1;
									ThemSinhVienDkCuoiDs(dsltc_Theo_Dk_User.ds[index]->dssvdk, svdk);
									dsltc_Theo_Dk_User.ds[index]->dssvdk.n++;
									BaoLoi("Dang ky thanh cong!");
								}
								else if (luachon == 2) {
									//Hủy đăng ký
									//Trước khi hủy đăng ký còn dò xem sinh viên này đã đk chưa
									bool check1 = false;
									for (NodeSvDangKy* k = dsltc_Theo_Dk_User.ds[index]->dssvdk.pHead; k != NULL; k = k->pNext) {
										if (k->Data.MaSv == value[0]) {
											check1 = true;
											break;
										}
									}
									if (check1 == false) {
										BaoLoi("Sinh vien nay chua dang ky lop!");
										break;
									}
									else {
										XoaSinhVienDkCuoiDs(dsltc_Theo_Dk_User.ds[index]->dssvdk);
										dsltc_Theo_Dk_User.ds[index]->dssvdk.n--;
										BaoLoi("Huy dang ky thanh cong!");
									}
								}
								//Đổ ngược dữ liệu từ dsltc có điều kiện về dsltc tổng
									//Chỗ lọc này hơi chậm, cần xem lại
								for (int i = 0; i < dsltc.n; i++) {
									if (dsltc.ds[i]->MaLopTc == dsltc_Theo_Dk_User.ds[index]->MaLopTc) {
										dsltc.ds[i] = dsltc_Theo_Dk_User.ds[index];
									}
								}
								break;
							}
						}
						Normal();
						ClearFrame3();
					}
				}
			}
		}
	}
}