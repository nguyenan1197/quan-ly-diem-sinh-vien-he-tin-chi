﻿#pragma once
#include "mylib.h"
#include "DoHoa.h"


//============= SINH VIÊN 
struct SinhVien {
	string MaSv;
	string Ho, Ten, Sdt, MaLop;
	char Phai; //0 - Nam, 1 - Nu
}; typedef struct SinhVien SINHVIEN;
struct NodeSinhVien {
	SINHVIEN Data;
	NodeSinhVien* pNext;
}; typedef struct NodeSinhVien* NODESV;
struct DanhSachSinhVien {
	NodeSinhVien* pHead = NULL;
	NodeSinhVien* pTail = NULL;
}; typedef struct DanhSachSinhVien DANHSACHSINHVIEN;
void HeaderDanhSachSinhVien() {
	SetColor(9); gotoxy(85, 2); cout << "DANH SACH SINH VIEN"; Normal();
	box(54, 3, 2, 85); box(54, 5, 38, 85); //Vẽ 2 box trên và dưới
	gotoxy(54, 5); cout << char(195); // Làm đẹp kk
	gotoxy(54 + 85, 5); cout << char(180); // Làm đẹp kk
	gotoxy(55, 4);
	cout << setw(15) << left << "MA SV";
	cout << setw(20) << left << "HO";
	cout << setw(10) << left << "TEN";
	cout << setw(5) << left << "PHAI";
	cout << setw(15) << left << "SDT";
	cout << setw(15) << left << "MA LOP";
}
NodeSinhVien* KhoiTaoNodeSinhVien(SINHVIEN sv) {
	NodeSinhVien* p = new NodeSinhVien;
	p->Data = sv;
	p->pNext = NULL;
	return p;
}
void ThemSinhVienCuoiDs(DANHSACHSINHVIEN& dssv, SINHVIEN sv) {
	NodeSinhVien* nodesv = KhoiTaoNodeSinhVien(sv);
	if (dssv.pHead == NULL)
		dssv.pHead = dssv.pTail = nodesv;
	else {
		dssv.pTail->pNext = nodesv;
		dssv.pTail = nodesv;
	}
}
int LoadFileDanhSachSinhVien(DANHSACHSINHVIEN& dssv, ifstream& FileIn) {
	//Xử lý mở file 
	string FileName = "danhsachsinhvien.txt";
	FileIn.open(FileName, ios_base::in);
	if (!FileIn) {
		BaoLoi("Khong tim thay File!");
		FileIn.close();
		exit(0);
	}
	if (FileEmpty(FileIn)) {
		FileIn.close();
		return 0;
	}
	//Đọc file vào danh sách
	while (!FileIn.eof()) {
		//Đọc vô sinh viên x, x là lớp đệm, rồi thêm x vô danh sách
		SINHVIEN x;
		getline(FileIn, x.MaSv, '\t');
		getline(FileIn, x.Ho, '\t');
		getline(FileIn, x.Ten, '\t');
		FileIn >> x.Phai;
		FileIn.ignore();
		getline(FileIn, x.Sdt, '\t');
		getline(FileIn, x.MaLop);
		// Đọc vô x rồi check cái đã rồi mới thêm vô danh sách
		/*Phát triển sau*/
		//Check xong gán lớp x cho thành phần trong danh sách
		ThemSinhVienCuoiDs(dssv, x);
	}
	FileIn.close();
	return 0;
}
void Nhap1SinhVien(SINHVIEN& sv) {
	cout << "\nNhap ma sv:"; cin >> sv.MaSv; while (getchar() != '\n');
	cout << "\nNhap ho sv:"; getline(cin, sv.Ho);
	cout << "\nNhap ten sv: "; getline(cin, sv.Ten);
	cout << "\nNhap phai: "; cin >> sv.Phai;
	cout << "\nNhap sdt: "; cin >> sv.Sdt;
	cout << "\nNhap ma lop: "; getline(cin, sv.MaLop);
}
void Xuat1SinhVien(SINHVIEN sv) {
	cout << setw(15) << left << sv.MaSv;
	cout << setw(20) << left << sv.Ho;
	cout << setw(10) << left << sv.Ten;
	cout << setw(5) << left << sv.Phai;
	cout << setw(15) << left << sv.Sdt;
	cout << setw(15) << left << sv.MaLop;
}
int XuatDanhSachSinhVien(DANHSACHSINHVIEN dssv, string maLop) {
	int count = 0;
	HeaderDanhSachSinhVien();
	int const x = 55; int y = 6;
	for (NodeSinhVien* k = dssv.pHead; k != NULL; k = k->pNext) {
		if (k->Data.MaLop == maLop) {
			gotoxy(x, y++);
			Xuat1SinhVien(k->Data);
			count++;
		}	
	}
	return count;
}
void ThemSinhVienVaoDauDs(DANHSACHSINHVIEN& dssv, SINHVIEN sv) {
	NodeSinhVien* nodesv = KhoiTaoNodeSinhVien(sv);
	if (dssv.pHead == NULL)
		dssv.pHead = dssv.pTail = nodesv;
	else {
		nodesv->pNext = dssv.pHead;
		dssv.pHead = nodesv;
	}
}
void XoaSinhVienDauDs(DANHSACHSINHVIEN& dssv) {
	if (dssv.pHead == NULL) return;
	NodeSinhVien* temp = dssv.pHead;
	dssv.pHead = dssv.pHead->pNext;
	delete temp;
}
void XoaSinhVienCuoiDs(DANHSACHSINHVIEN& dssv) {
	//TH Danh sách rỗng
	if (dssv.pHead == NULL) return;
	//TH Danh sách có phần tử
	if (dssv.pHead->pNext == NULL) XoaSinhVienDauDs(dssv); //Danh sách có 1 phần tử
	else { //Danh sách có từ 2 phần tử trở lên
		for (NodeSinhVien* k = dssv.pHead; k != NULL; k = k->pNext) {
			//Tìm thấy phần tử áp cuối
			if (k->pNext == dssv.pTail) {
				delete dssv.pTail; //Xóa phần tử cuối đi
				k->pNext = NULL; //Cho thằng áp cuối trỏ về NULL
				dssv.pTail = k; //Cập nhật lại pTail là k
				return; //Xong
			}
		}
	}
}
void XoaSinhVienTheoMaSv(DANHSACHSINHVIEN& dssv, string mssv) {
	if (dssv.pHead == NULL)
		cout << "\nDanh sach sinh vien rong!";
	else if (dssv.pHead->Data.MaSv == mssv)
		XoaSinhVienDauDs(dssv);
	else if (dssv.pTail->Data.MaSv == mssv)
		XoaSinhVienCuoiDs(dssv);
	else { //Không phải đầu, cũng không phải cuối
		for (NodeSinhVien* k = dssv.pHead; k->pNext != NULL; k = k->pNext) {
			//Nhớ là duyệt tới áp cuối thôi vì tìm thằng đứng trước
			//Nếu cho duyệt tới cuối thì next làm gì có => lỗi
			//Tìm được node trước sinh viên cần xóa (có mssv khớp)
			if (k->pNext->Data.MaSv == mssv) {
				NodeSinhVien* nodesv = k->pNext; // Cho 1 nodesv giữ nó
				k->pNext = nodesv->pNext; //Cho thằng trước trỏ tới thằng sau của thằng cần xóa
				delete nodesv; //Xóa thằng cần xóa
				return;
			}
		}
		//Chạy đến đây là không tìm thấy sinh viên có mã khớp rồi đó
		cout << "\nKhong tim thay sinh vien co ma: " << mssv; system("pause");
	}
}
void XuatThongTinSinhVienFrame2(SINHVIEN sv) {
	string title[] = { "LOP:", "MSSV:", "HO TEN:" };
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	value[0] = sv.MaLop;
	value[1] = sv.MaSv;
	value[2] = sv.Ho + " " + sv.Ten;
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t   THONG TIN SINH VIEN"; Normal();
	XuatDuLieu(5, 18, 2, 30, title, slData, value);
}

//============= SINH VIÊN ĐĂNG KÝ
struct SinhVienDangKy {
	string MaSv;
	float Diem;
	bool TrangThai; //0: Hủy; 1: Đã đăng ký
}; typedef struct SinhVienDangKy SINHVIENDK;
struct NodeSvDangKy {
	SINHVIENDK Data;
	NodeSvDangKy* pNext;
}; typedef struct NodeSvDangKy* NODESVDK;
struct DanhSachSinhVienDangKy {
	NodeSvDangKy* pHead = NULL;
	NodeSvDangKy* pTail = NULL;
	int n = 0;
}; typedef struct DanhSachSinhVienDangKy DSSVDK;
void HeaderDanhSachSinhVienDangKy() {
	SetColor(9); gotoxy(100, 2); cout << "DANH SACH SINH VIEN DANG KY"; Normal();
	box(54, 3, 2, 109); box(54, 5, 38, 109); //Vẽ 2 box trên và dưới
	gotoxy(54, 5); cout << char(195); // Làm đẹp kk
	gotoxy(54 + 109, 5); cout << char(180); // Làm đẹp kk
	gotoxy(55, 4);
	cout << setw(20) << left << "MA SV";
	cout << setw(35) << left << "HO TEN";
	cout << setw(20) << left << "LOP";
	cout << setw(10) << left << "DIEM";
	cout << setw(20) << left << "TRANG THAI";
}
NodeSvDangKy* KhoiTaoNodeSinhVienDangKy(SINHVIENDK svdk) {
	NodeSvDangKy* p = new NodeSvDangKy;
	p->Data = svdk;
	p->pNext = NULL;
	return p;
}
int Nhap1SinhVienDangKy(SinhVienDangKy &svdk) {
	ShowCur(1);
	string title[] = { "Ma SV: ", "DIEM: ", "TRANG THAI: " };
	//set thuộc tính cho từng field
	bool includeNum[] = { true, true, true }; //Có chứa số 
	bool includeAlphabet[] = { true, false, false }; //Có chứa chữ
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t     THEM LOP TIN CHI"; Normal();
	int len[slData] = { 19, 2, 1 }; //Độ dài quy định của từng field
	char key_press = 0;
	NhapDuLieu(5, 20, 2, 30, title, slData, value, includeNum, includeAlphabet, len, key_press);
	ShowCur(0);
	//Nhập xong rồi thì gán dữ liệu nè
	if (value->empty()) {
		return -1;
	}
	else {
		svdk.MaSv = value[0];
		svdk.Diem = atoi(value[1].c_str());
		svdk.TrangThai = atoi(value[2].c_str());
	}
	return 1;
}
void Xuat1SinhVienDangKy(SinhVienDangKy svdk, DANHSACHSINHVIEN dssv) {
	cout << setw(20) << left << svdk.MaSv;
	for (NodeSinhVien* k = dssv.pHead; k != NULL; k = k->pNext) {
		if (k->Data.MaSv == svdk.MaSv) {
			cout << setw(35) << left << k->Data.Ho + " " + k->Data.Ten;
			cout << setw(20) << left << k->Data.MaLop;
		}
	}
	cout << setw(10) << left << svdk.Diem;
	cout << setw(20) << left << svdk.TrangThai;
}
void XuatDanhSachSinhVienDangKy(DSSVDK dssvdk, DANHSACHSINHVIEN dssv) {
	HeaderDanhSachSinhVienDangKy();
	int const x = 55; int y = 6;
	for (NodeSvDangKy* k = dssvdk.pHead; k != NULL; k = k->pNext) {
		gotoxy(x, y++);
		Xuat1SinhVienDangKy(k->Data, dssv);
	}
}
void ThemSinhVienDkCuoiDs(DSSVDK& dssvdk, SINHVIENDK svdk) {
	NodeSvDangKy* nodesvdk = KhoiTaoNodeSinhVienDangKy(svdk);
	if (dssvdk.pHead == NULL)
		dssvdk.pHead = dssvdk.pTail = nodesvdk;
	else {
		dssvdk.pTail->pNext = nodesvdk;
		dssvdk.pTail = nodesvdk;
	}
}
void XoaSinhVienDkDauDs(DSSVDK& dssvdk) {
	if (dssvdk.pHead == NULL) return;
	NodeSvDangKy* temp = dssvdk.pHead;
	dssvdk.pHead = dssvdk.pHead->pNext;
	delete temp;
}
void XoaSinhVienDkCuoiDs(DSSVDK& dssvdk) {
	//TH Danh sách rỗng
	if (dssvdk.pHead == NULL) return;
	//TH Danh sách có phần tử
	if (dssvdk.pHead->pNext == NULL) XoaSinhVienDkDauDs(dssvdk); //Danh sách có 1 phần tử
	else { //Danh sách có từ 2 phần tử trở lên
		for (NodeSvDangKy* k = dssvdk.pHead; k != NULL; k = k->pNext) {
			//Tìm thấy phần tử áp cuối
			if (k->pNext == dssvdk.pTail) {
				delete dssvdk.pTail; //Xóa phần tử cuối đi
				k->pNext = NULL; //Cho thằng áp cuối trỏ về NULL
				dssvdk.pTail = k; //Cập nhật lại pTail là k
				return; //Xong
			}
		}
	}
}