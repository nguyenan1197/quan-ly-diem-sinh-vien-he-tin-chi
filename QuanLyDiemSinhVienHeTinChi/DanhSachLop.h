﻿#pragma once
#include "mylib.h"
#include "DoHoa.h"
struct Lop {
	string maLop = "", tenLop = "";
	int nienKhoa = 0;
}; typedef struct Lop LOP;
struct DanhSachLop {
	LOP ds[100];
	int n = 0;
}; typedef struct DanhSachLop DANHSACHLOP;
void HeaderDanhSachLop() {
	SetColor(9); gotoxy(85, 2); cout << "DANH SACH LOP"; Normal();
	box(54, 3, 2, 85); box(54, 5, 38, 85); //Vẽ 2 box trên và dưới
	gotoxy(54, 5); cout << char(195); // Làm đẹp kk
	gotoxy(54 + 85, 5); cout << char(180); // Làm đẹp kk
	gotoxy(55, 4);
	cout << setw(25) << left << "MA LOP";
	cout << setw(40) << left << "TEN LOP";
	cout << setw(10) << left << "NIEN KHOA";
}
int NhapLop(LOP &x) {
	ShowCur(1);
	string title[] = { "Ma Lop: ", "Ten Lop: ", "Nien khoa: " };
	//set thuộc tính cho từng field
	bool includeNum[] = { true, false, true }; //Có chứa số 
	bool includeAlphabet[] = { true, true, false }; //Có chứa chữ
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	//==== gán dữ liệu trước để show ra khi cần nhập lại
	value[0] = x.maLop;
	value[1] = x.tenLop;
	value[2] = to_string(x.nienKhoa);
	if (value[2] == "0")
		value[2].clear();
	//=================================================
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t     NHAP THONG TIN LOP"; Normal();
	int len[slData] = { 15, 25, 4 }; //Độ dài quy định của từng field
	char key_press = 0;
	NhapDuLieu(4, 20, 2, 30, title, slData, value, includeNum, includeAlphabet, len, key_press);
	ShowCur(0);
	//Nhập xong rồi thì gán dữ liệu nè
	//Trước khi gán thì kiểm tra coi nhập đủ chưa, chưa thì bắt nhập lại
	bool CheckEmpty = true;
	for (int i = 0; i < slData; i++) {
		if (!value[i].empty()) {
			CheckEmpty = false;
			break;
		}
	}
	if (CheckEmpty == true) return -1;
	else {
		x.maLop = value[0];
		x.tenLop = value[1];
		x.nienKhoa = atoi(value[2].c_str());
	}
	return 1;
}
void ThemLopVaoDanhSach(DANHSACHLOP& dsl, LOP x) {
	dsl.ds[dsl.n] = x;
	dsl.n++;
}
void XuatLop(LOP x) {
	cout << setw(25) << left << x.maLop;
	cout << setw(40) << left << x.tenLop;
	cout << setw(10) << left << x.nienKhoa;
}
void XuatDanhSachLop(DANHSACHLOP dsl) {
	HeaderDanhSachLop();
	int const x = 55; int y = 6;
	for (int i = 0; i < dsl.n; i++) {
		gotoxy(x, y++);
		XuatLop(dsl.ds[i]);
	}
}
int SearchLop(DANHSACHLOP dsl, string maLop) {
	for (int i = 0; i < dsl.n; i++) {
		if (dsl.ds[i].maLop == maLop)
			return i;
	}
	return -1;
}
int LoadFileDanhSachLop(DANHSACHLOP& dsl, ifstream& FileIn) {
	//Xử lý mở file 
	string FileName = "danhsachlop.txt";
	FileIn.open(FileName, ios_base::in);
	if (!FileIn) {
		BaoLoi("Khong tim thay File!");
		FileIn.close();
		exit(0);
	}
	if (FileEmpty(FileIn)) {
		FileIn.close();
		return 0;
	}
	//Đọc file vào danh sách
	while (!FileIn.eof()) {
		//Đọc vô lớp x, x là lớp đệm, rồi thêm x vô danh sách
		LOP x;
		getline(FileIn, x.maLop, '\t');
	    getline(FileIn, x.tenLop, '\t');
		FileIn >> x.nienKhoa;
		FileIn.ignore();
		// Đọc vô x rồi check cái đã rồi mới thêm vô danh sách
		if (SearchLop(dsl, x.maLop) != -1) {
			BaoLoi("Loi File, du lieu bi trung!");
			FileIn.close();
			exit(0);
		}
		//Check xong gán lớp x cho thành phần trong danh sách
		ThemLopVaoDanhSach(dsl, x);
	}
	FileIn.close();
	return 0;
}
int CapNhatLop(LOP& lop) {
	ShowCur(1);
	string title[] = { "Ma Lop: ", "Ten Lop: ", "Nien khoa: " };
	//set thuộc tính cho từng field
	bool includeNum[] = { true, false, true }; //Có chứa số 
	bool includeAlphabet[] = { true, true, false }; //Có chứa chữ
	const int slData = sizeof(title) / sizeof(title[0]);
	string value[slData];
	value[0] = lop.maLop;
	value[1] = lop.tenLop;
	value[2] = to_string(lop.nienKhoa);
	gotoxy(7, 16); SetColor(9);
	cout << "\t\t     CAP NHAT THONG TIN LOP"; Normal();
	int len[slData] = { 15, 25, 4 }; //Độ dài quy định của từng field
	char key_press = 0;
	NhapDuLieu(5, 20, 2, 30, title, slData, value, includeNum, includeAlphabet, len, key_press);
	ShowCur(0);
	//Nhập xong rồi thì gán dữ liệu nè
	//Trước khi gán thì kiểm tra coi nhập đủ chưa, chưa thì bắt nhập lại
	bool CheckEmpty = true;
	for (int i = 0; i < slData; i++) {
		if (!value[i].empty()) {
			CheckEmpty = false;
			break;
		}
	}
	if (CheckEmpty == true) return -1;
	else {
		lop.maLop = value[0];
		lop.tenLop = value[1];
		lop.nienKhoa = atoi(value[2].c_str());
	}
	return 1;
}
void XoaLop(DANHSACHLOP& dsl, string maLop) {
	int vt = SearchLop(dsl, maLop);
	for (int i = vt; i < dsl.n - 1; i++) {
		dsl.ds[i] = dsl.ds[i + 1];
	}
	dsl.n--;
	//Xử lý giao diện
	//Xóa hết mấy dòng dưới dòng lớp tín chỉ vừa xóa
	while (vt < dsl.n + 1) {
		if (vt == dsl.n) {
			for (int i = 55; i < 55 + 104; i++) {
				gotoxy(i, 6 + vt); cout << " ";
			}
			break;
		}
		vt++;
		for (int i = 55; i < 55 + 104; i++) {
			gotoxy(i, 6 + vt); cout << " ";
		}
	}
	XuatDanhSachLop(dsl);
}
void HeaderBangDiemTongKet(string maLop, DanhSachLopTinChi dsltc) {
	SetColor(9); gotoxy(85, 2); cout << "BANG DIEM TONG KET - " << maLop; Normal();
	box(54, 3, 2, 109); box(54, 5, 38, 109); //Vẽ 2 box trên và dưới
	gotoxy(54, 5); cout << char(195); // Làm đẹp kk
	gotoxy(54 + 109, 5); cout << char(180); // Làm đẹp kk
	gotoxy(55, 4);
	cout << setw(5) << left << "STT";
	cout << setw(15) << left << "MA SV";
	cout << setw(20) << left << "HO";
	cout << setw(10) << left << "TEN";
	for (int i = 0; i < dsltc.n; i++) {
		if (dsltc.ds[i]->TrangThai == 1) {
			cout << setw(10) << left << dsltc.ds[i]->MaMh;
		}
	}
}
void XuatDiem1SinhVien(SINHVIEN sv) {
	cout << setw(5) << left << 1;
	cout << setw(15) << left << sv.MaSv;
	cout << setw(20) << left << sv.Ho;
	cout << setw(10) << left << sv.Ten;
}
void XuatDiem1Lop(DANHSACHSINHVIEN dssv, string maLop, DanhSachLopTinChi dsltc) {
	HeaderBangDiemTongKet(maLop, dsltc);
	int const x = 55; int y = 6;
	for (NodeSinhVien* k = dssv.pHead; k != NULL; k = k->pNext) {
		if (k->Data.MaLop == maLop) {
			gotoxy(x, y++);
			XuatDiem1SinhVien(k->Data);
			for (int i = 0; i < dsltc.n; i++) {
				for (NodeSvDangKy* m = dsltc.ds[i]->dssvdk.pHead; m != NULL; m = m->pNext) {
					if (m->Data.MaSv == k->Data.MaSv)
						cout << setw(10) << left << m->Data.Diem;
				}
			}
		}
	}
}
void XuLyMenuDanhSachLop(DANHSACHLOP& dsl, DANHSACHSINHVIEN& dssv, DanhSachLopTinChi dsltc) {
	ClearAll();
	MenuFrameCap1();
	bool flag = true;
	while (flag) {
		ClearFrame2();
		ClearFrame3();
		XuatDanhSachLop(dsl);
		string td[] = { "THEM LOP MOI          ",
						"CAP NHAT THONG TIN LOP",
						"XOA LOP               ",
						"XEM DSSV              ",
						"XEM BANG DIEM TONG KET" };
		int so_item = sizeof(td) / sizeof(td[0]);
		int luachon = MenuDong(2, 4, td, so_item);
		if (luachon == 0) flag = false;
		//Thêm lớp
		else if (luachon == 1) {
			LOP x;
			do {
				//ClearFrame2();
				if (NhapLop(x) != -1) {
					if (x.maLop == "")
						BaoLoi("Ma lop khong duoc bo trong!");
					else if (x.tenLop == "")
						BaoLoi("Ten lop khong duoc bo trong!");
					else if (x.nienKhoa == 0)
						BaoLoi("Nien khoa khong duoc bo trong!");
					else if (SearchLop(dsl, x.maLop) != -1) {
						BaoLoi("Ma lop bi trung, xin kiem tra lai!");
					}
					else {
						ThemLopVaoDanhSach(dsl, x);
						BaoLoi("Them thanh cong!");
						break;
					}
				}
				else break;
			} while (SearchLop(dsl, x.maLop) != -1 || x.maLop == "" || x.tenLop == "" || x.nienKhoa == 0);
		}
		//Cập nhật thông tin lớp
		else if (luachon == 2) {
			BaoLoi("Chuc nang dang phat trien!");
		}
		//Xóa lớp
		else if (luachon == 3) {
			//Xóa khi lớp không có danh sách sinh viên nhé
			if (dsl.n == 0) {
				BaoLoi("Danh sach lop rong!");
				continue;
			}
			int index = 0;
			gotoxy(55, 6);
			SetBGColor(2);
			XuatLop(dsl.ds[index]);
			char key_press_test = 0;
			while (key_press_test != ESC) {
				key_press_test = _getch();
				if (key_press_test == -32) {
					key_press_test = _getch();
					if (key_press_test == DOWN && index < dsl.n - 1) {
						gotoxy(55, 6 + index);
						Normal();
						XuatLop(dsl.ds[index]);
						index++;
					}
					else if (key_press_test == UP && index > 0) {
						gotoxy(55, 6 + index);
						Normal();
						XuatLop(dsl.ds[index]);
						index--;
					}
				}
				gotoxy(55, 6 + index);
				SetBGColor(2);
				XuatLop(dsl.ds[index]);
				if (key_press_test == ENTER)
					break;
			}
			Normal();
			if (key_press_test != ESC) {
				ClearFrame3();
				if (XuatDanhSachSinhVien(dssv, dsl.ds[index].maLop) > 0) {
					BaoLoi("Lop da co du lieu SV, khong the xoa!");
					continue;
				}
				char luachon = 0;
				do {
					ClearFrame2();
					gotoxy(5, 44); cout << "Xac nhan xoa lop tin chi ? (y/n)";
					luachon = _getch();
					if (luachon == 'y' || luachon == 'Y') {
						XoaLop(dsl, dsl.ds[index].maLop);
						ClearFrame2();
						BaoLoi("Xoa thanh cong!");
					}
					else if (luachon == 'n' || luachon == 'N') break;
					else BaoLoi("Lua chon khong hop le!");
				} while (luachon != 'y' && luachon != 'Y' && luachon != 'n' && luachon != 'N');
			}
		}
		//Xem danh sách lớp
		else if (luachon == 4) {
			if (dsl.n == 0) {
				BaoLoi("Danh sach lop rong!");
				continue;
			}
			int index = 0;
			gotoxy(55, 6);
			SetBGColor(2);

			XuatLop(dsl.ds[index]);
			char key_press_test = 0;
			while (key_press_test != ESC) {
				key_press_test = _getch();
				if (key_press_test == -32) {
					key_press_test = _getch();
					if (key_press_test == DOWN && index < dsl.n - 1) {
						gotoxy(55, 6 + index);
						Normal();
						XuatLop(dsl.ds[index]);
						index++;
					}
					else if (key_press_test == UP && index > 0) {
						gotoxy(55, 6 + index);
						Normal();
						XuatLop(dsl.ds[index]);
						index--;
					}
				}
				gotoxy(55, 6 + index);
				SetBGColor(2);
				XuatLop(dsl.ds[index]);
				//Di chuyển thanh sáng đến khi gặp phím Enter thì break
				if (key_press_test == ENTER)
					break;
			}
			Normal();
			//Break ra xử lý ở đây
			ClearFrame3();
			XuatDanhSachSinhVien(dssv, dsl.ds[index].maLop);
			_getch();
		}
		//Xem bảng điểm tổng kết
		else if (luachon == 5) {
			XuatDanhSachLop(dsl);
			int index = 0;
			gotoxy(55, 6);
			SetBGColor(2);

			XuatLop(dsl.ds[index]);
			char key_press_test = 0;
			while (key_press_test != ESC) {
				key_press_test = _getch();
				if (key_press_test == -32) {
					key_press_test = _getch();
					if (key_press_test == DOWN && index < dsl.n - 1) {
						gotoxy(55, 6 + index);
						Normal();
						XuatLop(dsl.ds[index]);
						index++;
					}
					else if (key_press_test == UP && index > 0) {
						gotoxy(55, 6 + index);
						Normal();
						XuatLop(dsl.ds[index]);
						index--;
					}
				}
				gotoxy(55, 6 + index);
				SetBGColor(2);
				XuatLop(dsl.ds[index]);
				//Di chuyển thanh sáng đến khi gặp phím Enter thì break
				if (key_press_test == ENTER) {
					//Break ra xử lý ở đây
					Normal();
					ClearFrame3();
					XuatDiem1Lop(dssv, dsl.ds[index].maLop, dsltc);
					_getch();
					break;
				}
			}
		}
	}
}
